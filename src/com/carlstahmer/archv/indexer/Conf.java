/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */
package com.carlstahmer.archv.indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.yaml.snakeyaml.Yaml;

/**
 * @author cstahmer
 * @author Nigel Pierce
 *  
 * <p>A configuration class that contains code for processing a .yml configuration file
 * and loading all configuration variables into class objects.  By creating a local
 * instance of this class or being passed an instance, other classes and functions gain
 * access to configuration variable values.</p>
 */
public class Conf {

	String listenDir;
	String writeDir;
	String dbserver;
	String dbname;
	String dbuser;
	String dbpass;
	boolean debug;
	boolean console;
	boolean frequencyCalc;
	boolean optimizeFiles;
	Integer usethreshold;
	Integer fileoccurrencethreshold;
	boolean makeluceneindex;
	String lucenereaddir;
	String lucenewritedir;
	Boolean lucenesearch;
	String searchseed;
	Integer searchlucenerelevancethreshold;
	Integer searchrelativerelevancethreshold;
	Boolean searchsortlucene;
	Integer searchmaxreturn;
	String searchluceneindex;
	Integer humancataloguefactor;
	boolean addcataloguewords;
	
	HashMap<String, Object> confFileState; // the state of config.yml
	
	public Conf() {
		listenDir = "";
		writeDir = "";
		dbserver = "";
		dbname = "";
		dbuser = "";
		dbpass = "";
		debug = false;
		console = false;
		frequencyCalc = false;
		optimizeFiles = false;
		usethreshold = 50000;
		fileoccurrencethreshold = 4000;
		makeluceneindex = false;
		lucenereaddir = "";
		lucenewritedir = "";
		lucenesearch = false;
		searchseed = "don\'t mind me";
		searchlucenerelevancethreshold = 0;
		searchrelativerelevancethreshold = 0;
		searchsortlucene = true;
		searchmaxreturn = 20;
		searchluceneindex = "";
		confFileState = new HashMap<String, Object>();
		humancataloguefactor = 1;
		addcataloguewords = false;
	}
	
	/**
	 * <p>An initialization class that tells the object to read the .yml configuration file
	 * and load all values.  Must be called before trying to access any class properties.</p>
	 * 
	 * @return      			a boolean value indicating whether the configuration .yml has been successfully loaded.
	 */
	public boolean loadConf() {
			
		boolean loaded = false;
		// Load Configuration YAML
		try {
			
			InputStream yamlInput = new FileInputStream(new File("config.yml"));
			System.out.println("Found configuration file...");
			Yaml yaml = new Yaml();
			
			confFileState.putAll(new HashMap<String, Object>());
			
			@SuppressWarnings("unchecked")
			
			HashMap<String, Object> tempMap = new HashMap<String, Object>();
			tempMap.putAll((Map<String, Object>) yaml.load(yamlInput));
			confFileState.putAll(tempMap);
			
			listenDir = (String) getConfFileValIfDefined("listendir", listenDir);
			writeDir = (String) getConfFileValIfDefined("writedir", writeDir);
			dbserver = (String) getConfFileValIfDefined("dbserver", dbserver);
			dbname = (String) getConfFileValIfDefined("dbname", dbname);
			dbuser = (String) getConfFileValIfDefined("dbuser", dbuser);
			dbpass = (String) getConfFileValIfDefined("dbpass", dbpass);
			usethreshold = (Integer) getConfFileValIfDefined("usethreshold", usethreshold);
			fileoccurrencethreshold = (Integer) getConfFileValIfDefined("fileoccurrencethreshold", fileoccurrencethreshold);
			lucenereaddir = (String) getConfFileValIfDefined("lucenereaddir", lucenereaddir);
			lucenewritedir = (String) getConfFileValIfDefined("lucenewritedir", lucenewritedir);
			searchseed = (String) getConfFileValIfDefined("searchseed", searchseed);
			searchlucenerelevancethreshold = (Integer) getConfFileValIfDefined("searchlucenerelevancethreshold", searchlucenerelevancethreshold);
			searchrelativerelevancethreshold = (Integer) getConfFileValIfDefined("searchrelativerelevancethreshold", searchrelativerelevancethreshold);
			searchsortlucene = (Boolean) getConfFileValIfDefined("searchsortlucene", searchsortlucene);
			searchmaxreturn = (Integer) getConfFileValIfDefined("searchmaxreturn", searchmaxreturn);
			searchluceneindex = (String) getConfFileValIfDefined("searchluceneindex", searchluceneindex);
			humancataloguefactor = (Integer) getConfFileValIfDefined("humancataloguefactor", humancataloguefactor);
			loaded = true;
		
		} catch (FileNotFoundException e) {
		    System.err.println("Configuration FileNotFoundException: " + e.getMessage());
		    loaded = false;
		}
		
		return loaded;
	}
	
	/**
	 * <p>Checks for passed command line arguments and replaces .yml loaded values with those
	 * entered at the command line as appropriate.</p>
	 *
	 * @param  args    	command line String[] args array
	 * @return      	a boolean value indicate whether or not method executed successfully
	 */
	public boolean checkArgs(String[] args) {
		
		boolean loaded = false;
	
		try {
			Options options = new Options();
			options.addOption("listendir", true, "full directory path to listen directory");
			options.addOption("writedir", true, "full directory path to write output records");
			options.addOption("dbserver", true, "the sql server");
			options.addOption("dbname", true, "the sql server database name");
			options.addOption("dbuser", true, "the sql user");
			options.addOption("dbpass", true, "the sql user");
			options.addOption("debug", false, "run in debug mode - verbose logging");
			options.addOption("console", false, "write log to console instead of database");
			options.addOption("frequency", false, "performs a frequency calculation of visual words in the library");
			options.addOption("optimizefiles", false, "generate optimized visual word files");
			options.addOption("usethreshold", true, "max times word can appear for it to be included in optimized word files");
			options.addOption("fileoccurrencethreshold", true, "max number of files word can appear in for word to be included in optimized word files");
			options.addOption("makeluceneindex", false, "make a Lucene index of the visual words");
			options.addOption("lucenereaddir", true, "directory of viswords to index from");
			options.addOption("lucenewritedir", true, "directory the index will live in");
			// new ones 2014-06-21/22 :)
			options.addOption("lucenesearch", false, "Perform a search with a Lucene index");
			options.addOption("searchseed", true, "file to use as the basis for the search");
			options.addOption("searchlucenerelevancethreshold", true, "minimum absolute score for search");
			options.addOption("searchrelativerelevancethreshold", true, "minimum percent score for search");
			options.addOption("searchsortlucene", true, "sort based on relevance (true) (default), or percent relevance (false)");
			options.addOption("searchmaxreturn", true, "maximum number of results to return");
			options.addOption("searchluceneindex", true, "directory of index to search against");
			options.addOption("humancataloguefactor", true, "number of times to add each human catalogued entry to the visual word file");
			options.addOption("addcataloguewords", false, "add the human cataloguing words to the visual word file");
			options.addOption("help", false, "get help");
			
			CommandLineParser parser = new BasicParser();
			CommandLine cmd = parser.parse( options, args);
			if(cmd.hasOption("listendir")) {
				String ldirVal = cmd.getOptionValue("listendir");
				if(ldirVal != null) {
					listenDir = ldirVal;
				}
			} 
			if (cmd.hasOption("writedir")) {
				String wdirVal = cmd.getOptionValue("writedir");
				if(wdirVal != null) {
					writeDir = wdirVal;
				}
			}
			if (cmd.hasOption("dbserver")) {
				String srvVal = cmd.getOptionValue("dbserver");
				if(srvVal != null) {
					dbserver = srvVal;
				}
			}
			if (cmd.hasOption("dbname")) {
				String dbnameVal = cmd.getOptionValue("dbname");
				if(dbnameVal != null) {
					dbname = dbnameVal;
				}
			}
			if (cmd.hasOption("dbuser")) {
				String dbuserVal = cmd.getOptionValue("dbuser");
				if(dbuserVal != null) {
					dbuser = dbuserVal;
				}
			}
			if (cmd.hasOption("dbpass")) {
				String dbpassVal = cmd.getOptionValue("dbpass");
				if(dbpassVal != null) {
					dbpass = dbpassVal;
				}
			}
			if (cmd.hasOption("debug")) {
				debug = true;
			}
			if (cmd.hasOption("console")) {
				console = true;
			}
			if (cmd.hasOption("frequency")) {
				frequencyCalc = true;
			}			
			if (cmd.hasOption("optimizefiles")) {
				optimizeFiles = true;
			}
			if (cmd.hasOption("usethreshold")) {
				Integer usethresholdVal = Integer.valueOf(cmd.getOptionValue("usethreshold"));
				if (usethresholdVal != null) {
					usethreshold = usethresholdVal;
				}
			}
			if (cmd.hasOption("fileoccurrencethreshold")) {
				Integer fileoccurrencethresholdVal = Integer.valueOf(cmd.getOptionValue("fileoccurrencethreshold"));
				if (fileoccurrencethresholdVal != null) {
					fileoccurrencethreshold = fileoccurrencethresholdVal;
				}
			}
			if (cmd.hasOption("makeluceneindex")) {
				makeluceneindex = true;
			}
			if (cmd.hasOption("addcataloguewords")) {
				addcataloguewords = true;
			}			
			if (cmd.hasOption("lucenereaddir")) {
				String lucenereaddirVal = cmd.getOptionValue("lucenereaddir");
				if (lucenereaddirVal != null) {
					lucenereaddir = lucenereaddirVal;
				}
			}
			if (cmd.hasOption("lucenewritedir")) {
				String lucenewritedirVal = cmd.getOptionValue("lucenewritedir");
				if (lucenewritedirVal != null) {
					lucenewritedir = lucenewritedirVal;
				}
			}
			if (cmd.hasOption("lucenesearch")) {
				lucenesearch = true;
			}
			if (cmd.hasOption("searchseed")) {
				String searchseedVal = cmd.getOptionValue("searchseed");
				if (searchseedVal != null) {
					searchseed = searchseedVal;
				}
			}
			if (cmd.hasOption("searchlucenerelevancethreshold")) {
				Integer thresholdVal = Integer.valueOf(cmd.getOptionValue("searchlucenerelevancethreshold"));
				if (thresholdVal != null) {
					searchlucenerelevancethreshold = thresholdVal;
				}
			}
			if (cmd.hasOption("searchrelativerelevancethreshold")) {
				Integer thresholdVal = Integer.valueOf(cmd.getOptionValue("searchrelativerelevancethreshold"));
				if (thresholdVal != null) {
					searchrelativerelevancethreshold = thresholdVal;
				}
			}
			if (cmd.hasOption("searchsortlucene")) {
				Boolean sort = Boolean.valueOf(cmd.getOptionValue("searchsortlucene"));
				if (sort != null) {
					searchsortlucene = sort;
				}
			}
			if (cmd.hasOption("searchmaxreturn")) {
				Integer max = Integer.valueOf(cmd.getOptionValue("searchmaxreturn"));
				if (max != null) {
					searchmaxreturn = max;
				}
			}
			if (cmd.hasOption("searchluceneindex")) {
				String dir = cmd.getOptionValue("searchluceneindex");
				if (dir != null) {
					searchluceneindex = dir;
				}
			}
			if (cmd.hasOption("humancataloguefactor")) {
				Integer humanCatalogueFactorVal = Integer.valueOf(cmd.getOptionValue("humancataloguefactor"));
				if (humanCatalogueFactorVal != null) {
					humancataloguefactor = humanCatalogueFactorVal;
				}
			}
			if (cmd.hasOption("help")) {
					String HelpString = "Requires the presence of a config.yml in the application root to run correctly. ";
					HelpString = HelpString + "Values in the config can be overwritten at runtime via command line ";
					HelpString = HelpString + "arguments as follows:\n\n";
					HelpString = HelpString + "-listendir [/directory/path/of/listen/directory]\n";
					HelpString = HelpString + "-writedir [/directory/path/of/output/directory]\n";
					HelpString = HelpString + "-dbserver [the sql database server]\n";
					HelpString = HelpString + "-dbname [the sql database name]\n";
					HelpString = HelpString + "-dbuser [the sql database user]\n";
					HelpString = HelpString + "-dbpass [the sql database password]\n";
					HelpString = HelpString + "-debug [runs application in debug mode - verbose logging]\n";
					HelpString = HelpString + "-console [writes log output to console instead of database]\n";
					HelpString = HelpString + "-frequency [performs a frequency calculation of visual words in the library]\n";
					HelpString = HelpString + "-optimizefiles [generates optimized visual word files]\n";
					HelpString = HelpString + "-usethreshold [max times word can appear for it to be included in optimized word files]\n";
					HelpString = HelpString + "-fileoccurrencethreshold [max number of files word can appear in for word to be included in optimized word files]\n";
					HelpString = HelpString + "-makeluceneindex [make a Lucene index of the visual words]\n";
					HelpString = HelpString + "-lucenereaddir [directory of viswords to index from]\n";
					HelpString = HelpString + "-lucenewritedir [directory the index will live in]\n";
					HelpString = HelpString + "-lucenesearch [Perform a search with a Lucene index]\n";
					HelpString = HelpString + "-searchseed [file to use as the basis for the search]\n";
					HelpString = HelpString + "-searchlucenerelevancethreshold [minimum absolute score for search]\n";
					HelpString = HelpString + "-searchrelativerelevancethreshold [minimum percent score for search]\n";
					HelpString = HelpString + "-searchsortlucene [sort based on relevance (true) (default), or percent relevance (false)]\n";
					HelpString = HelpString + "-searchmaxreturn [maximum number of results to return]\n";
					HelpString = HelpString + "-searchluceneindex [directory of index to search against]\n";
					HelpString = HelpString + "-humancataloguefactor [number of times to add each catalgoued tag to visual word file]\n";
					HelpString = HelpString + "-help [runs this help message]\n\n";
					HelpString = HelpString + "Config.yml file must be in place even if you are supplying information ";
					HelpString = HelpString + "via the command line.";
					System.out.println(HelpString);
					System.out.println("listenDir: "+listenDir+"; writeDir: "+writeDir+"; etc searchseed: "+searchseed+"; searchsortlucene: "+searchsortlucene);
					System.exit(0);
			}

			
			loaded = true;
			
		}  catch (ParseException e) {
			System.err.println("ERROR:\tCommand Line Argument Error: " + e.getMessage());
			loaded = false;
		}
			
		return loaded;	
			
	}
	
	/**
	 * <p>Get value for optionName from config.yml, unless it's not defined in conf.yml 
	 * (in which case return option)</p>
	 * <p>The purpose is so that undefined options in config.yml don't override the defaults
	 * (i.e. setting option to null)</p>
	 * 
	 * @param	optionName	Name of the option as it would be in config.yml
	 * @param	option		Actual option member to modify
	 * @return
	 */
    Object getConfFileValIfDefined(String optionName, Object option) {
            Object optionVal = confFileState.get(optionName);
            if (optionVal != null) {
                    return optionVal;
            }
            else {
                    return option;
            }
    }
	
}
