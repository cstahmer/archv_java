/**
 * 
 */
package com.carlstahmer.archv.indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

/**
 * @author Nigel Pierce
 *
 * Initializes Lucene index for viswords, putting it in lucenewritedir specified in config.
 */
public class MakeLuceneIndex {

	Conf config;
	SqlModel sqlObj;
	Logger logger;
	FileUtils fileUtil;
	IndexWriter writer;
	
	/**
	 * Initializes object's members (doesn't make lucene index or anything)
	 * 
	 * @param	confObject	configuration -- uses lucenereaddir and lucenewritedir
	 * @param	sqlModObj	Connection to db
	 */
	public MakeLuceneIndex(Conf confObject, SqlModel sqlModObj) {
		config = confObject;
		sqlObj = sqlModObj;
		logger = new Logger(config);
		fileUtil = new FileUtils(config, sqlObj);
		writer = null;
	}
	
	/**
	 * <p>creates new Lucene index and fills it with optimized visual word data.</p>
	 * 
	 */
	public void run() {
		
		if (!requiredConfigPresent()) {
			System.out.println("Lucene read directory or write directory not configured. Aborting...");
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Lucene read directory or write directory not configured.");
			return;
		}
		
		try {
			
			makeIndexWriter();
		
			generateIndex();
			
		}
		catch (java.io.IOException e) {
			// do nothing but log it I guess
			logger.log(1,
				Thread.currentThread().getStackTrace()[1].getFileName(), 
				Thread.currentThread().getStackTrace()[1].getLineNumber(), 
				"Can't create index.");
			System.out.println("Can't create index. Aborting...");
			return;
		}
		finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					logger.log(1, 
						Thread.currentThread().getStackTrace()[1].getFileName(), 
						Thread.currentThread().getStackTrace()[1].getLineNumber(), 
						"Couldn't close index writer.");
				}
			}
		}
	}
	
	/**
	 * <p>Makes sure that Lucene read dir and write dir are both defined. Does 
	 * not check if specified directories exist.</p>
	 * 
	 * @return	whether they are both defined
	 */
	private boolean requiredConfigPresent() {
		return (config.lucenereaddir != null && config.lucenewritedir != null);
	}
	
	/**
	 * <p>Initialize the Lucene index writer. Uses a StandardAnalyzer; writes 
	 * to config.lucenewritedir</p>
	 * 
	 * @throws	java.io.IOException	If it cannot open the write directory
	 */
	private void makeIndexWriter() throws java.io.IOException {
		File readDir = new File(config.lucenewritedir);
		IndexWriterConfig writerConfig = new IndexWriterConfig(Version.LUCENE_47, new StandardAnalyzer(Version.LUCENE_47));
		writerConfig.setOpenMode(OpenMode.CREATE);
		writer = new IndexWriter(FSDirectory.open(readDir), writerConfig);
	}
	
	/**
	 * <p>Fills up the index with visword data.</p>
	 * 
	 */
	private void generateIndex() {
		fileUtil.listFilesWhere(config.lucenereaddir, "txt");
		
		logger.log(3,
			Thread.currentThread().getStackTrace()[1].getFileName(), 
			Thread.currentThread().getStackTrace()[1].getLineNumber(), 
			"Files to index: " + fileUtil.fileList);
		
		for (String fileName : fileUtil.fileList) {
			String filePath = config.lucenereaddir + fileName;
			System.out.println("Indexing "+filePath);
			try {
				String contents = fileUtil.openAsString(filePath);
				
				Document luceneDoc = new Document();
				luceneDoc.add(new TextField("viswords", contents, Field.Store.YES));
				luceneDoc.add(new TextField("filename", fileName, Field.Store.YES));
				
				writer.addDocument(luceneDoc);
				
			} catch (FileNotFoundException e) {
				logger.log(1, 
						Thread.currentThread().getStackTrace()[1].getFileName(), 
						Thread.currentThread().getStackTrace()[1].getLineNumber(), 
						config.lucenereaddir+"/" +fileName+" not found.");
			} catch (IOException e) {
				e.printStackTrace();
				logger.log(1, 
						Thread.currentThread().getStackTrace()[1].getFileName(), 
						Thread.currentThread().getStackTrace()[1].getLineNumber(), 
						"Unable to read from file "+config.lucenereaddir+"/"+fileName);
			}
		}
	}
}
