/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */
package com.carlstahmer.archv.indexer;

/**
 * @author cstahmer
 * 
 * <p>A logging object for handling system messaging.</p>
 */
public class Logger {
	
	Conf config;
	SqlModel sqlObj;
	
	/**
	 * <p>Constructor class that assigns passed Config object and SqlModel to 
	 * class instances.</p>
	 *
	 * @param  configObj    an instance of the Conf class
	 * @param  sqlModObj  	an instance of the sqlModel class
	 */
	public Logger(Conf configObj) {
		config = configObj;
		sqlObj = new SqlModel(config);
	}
	
	/**
	 * <p>Initiates process of writing a message to the log.
	 * Determines whether to log to console or sql and sends
	 * message to appropriate helper method.</p>
	 *
	 * @param  	messagetype    	[1] = error, [2] = info, [3] = debug
	 * @param 	filename		the filename of the file making the call
	 * @param 	linenumber		the line number of the call
	 * @param 	message			the message to log
	 */
	public void log(int messagetype, String filename, int linenumber, String message) {
		boolean shouldPrint = false;
		if (config.debug) {
			shouldPrint = true;
		} else if (messagetype == 1 || messagetype == 2) {
			shouldPrint = true;
		}
		if (shouldPrint) {
			if (config.console) {
				printToConsole(messagetype, filename, linenumber, message);
			} else {
				printToSql(messagetype, filename, linenumber, message);
			}
		}	
	}
	
	
	/**
	 * <p>Prints a log message to the console.</p>
	 *
	 * @param  	messagetype    	[1] = error, [2] = info, [3] = debug
	 * @param 	filename		the filename of the file making the call
	 * @param 	linenumber		the line number of the call
	 * @param 	message			the message to log
	 */
	private void printToConsole(int messagetype, String filename, int linenumber, String message) {
		String strMessageType;
		if (messagetype == 1) {
			strMessageType = "ERROR";
		} else if (messagetype == 2) {
			strMessageType = "INFO";
		} else if (messagetype == 3) {
			strMessageType = "DEBUG";
		} else {
			strMessageType = "OTHER";
		}
		System.out.println(strMessageType + ":\t" + filename + "\t" + linenumber + "\t" + message);
	}
	
	/**
	 * <p>Prints log message to sql</p>
	 *
	 * @param  	messagetype    	[1] = error, [2] = info, [3] = debug
	 * @param 	filename		the filename of the file making the call
	 * @param 	linenumber		the line number of the call
	 * @param 	message			the message to log
	 */
	private void printToSql(int messagetype, String filename, int linenumber, String message) {
		boolean submitSuccess = false;
		// fixify message with an apostrophe
		message = message.replace("\'", "\\\'");
		int retID = sqlObj.insertLogMessage(messagetype, filename, linenumber, message);
		if (retID > 0) {
			submitSuccess = true;
		}
		if (!submitSuccess) {
			printToConsole(1, filename, linenumber, "Logger has no active SQL connection, or other SQL error.  Original message ["+message+"]");
		}
	}
	

}
