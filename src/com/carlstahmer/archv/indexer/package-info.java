/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */

/**
 * @author cstahmer
 *
 */
package com.carlstahmer.archv.indexer;