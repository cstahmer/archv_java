package com.carlstahmer.archv.indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexNotFoundException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

/**
 * <p>Performs a search of Lucene index with query based on a visual word file</p>
 * 
 * @author Nigel Pierce
 *
 */
public class LuceneSearch {
	Conf config;
	Logger logger;
	FileUtils fileUtil;
	IndexReader reader;
	SqlModel sqlObj;
	
	/**
	 * Initializes object's members
	 * 
	 * @param	confObject	configuration -- uses lucenereaddir and lucenewritedir
	 * @param	sqlModObj	Connection to db
	 */
	public LuceneSearch(Conf confObject, SqlModel sqlModObj) {
		config = confObject;
		sqlObj = sqlModObj;
		logger = new Logger(config);
		fileUtil = new FileUtils(config, sqlObj);	
		reader = null;
	}
	
	/**
	 * <p>Class to hold a bit more information than the regular ScoreDoc holds</p>
	 * 
	 * @author	Nigel Pierce
	 *
	 */
	private class ScorePercentDoc extends ScoreDoc {
		
		String filename;
		float percentScore;
		
		/**
		 * <p>Constructor that the pappy requires</p>
		 * 
		 * @param	doc		The id of the document
		 * @param	score	The document's result score
		 */
		public ScorePercentDoc(int doc, float score) {
			super(doc, score);
			filename = "";
			percentScore = 0;
		}
		
		/**
		 * <p>Constructor used to initialize the fields meaningfully</p>
		 * 
		 * @param	scoreDoc		The ScoreDoc this object will be based on
		 * @param	maxPercentScore	The maximum score in the results
		 * @param	newFilename		The filename of the visword file it represents
		 */
		public ScorePercentDoc(ScoreDoc scoreDoc, float maxPercentScore, 
		String newFilename) {
			super(scoreDoc.doc, scoreDoc.score);
			
			filename = newFilename;
			percentScore = score / maxPercentScore * 100;
		}
		
		/**
		 * <p>For outputting to the console and such</p>
		 */
		public String toString() {
			return "doc="+doc+" filename "+filename+" score="+score+" percent="+percentScore+" shardIndex="+shardIndex;
		}
		
	}

	// I'm keeping this here in case a future schmuck jumbles up the results
	/**
	 * <p>Class to compare / sort ScorePercentDocs</p>
	 * 
	 * @author	Nigel Pierce
	 *
	 */
	private class ResultDocsComparator implements Comparator {

		boolean sortByRelevance;	// true: sort by score; false: sort by percentScore
		
		public ResultDocsComparator() {
			sortByRelevance = true;
		}
		
		public ResultDocsComparator(boolean newSortByRelevance) {
			sortByRelevance = newSortByRelevance;
		}
		
		@Override
		public int compare(Object arg0, Object arg1) {
			// TODO Auto-generated method stub
			if (arg0.getClass() == ScorePercentDoc.class && arg1.getClass() == ScorePercentDoc.class) {
				float arg0Val = 0;
				float arg1Val = 0;
				if (sortByRelevance) {
					arg0Val = ((ScorePercentDoc)arg0).score;
					arg1Val = ((ScorePercentDoc)arg1).score;
				}
				else {
					arg0Val = ((ScorePercentDoc)arg0).percentScore;
					arg1Val = ((ScorePercentDoc)arg1).percentScore;
				}
				
				if (arg0Val < arg1Val) {
					return 1;
				}
				else if (arg0Val == arg1Val) {
					return 0;
				}
				else if (arg0Val > arg1Val) {
					return -1;
				}
			}
			return 0;
		}
	
	}
	
	
	/**
	 * <p>Searches visword files (in Lucene index) for viswords in file 
	 * specified by config.searchseed</p>
	 * 
	 * @return	JSON array representation of the visword files; OR empty string 
	 * 			if error occurred.
	 */
	public String search() {
		
		String searchFileName = config.searchseed;
		System.out.println("File to open: "+searchFileName);
		String contents = null;
		
		try {
			
			contents = fileUtil.openAsString(searchFileName);

			reader = DirectoryReader.open(FSDirectory.open(new File(config.searchluceneindex)));
			
		} catch (IndexNotFoundException e) {
			String message = "Index not found in: "+config.searchluceneindex
					+"; possible misconfiguration.";
			System.out.println(message);
			logger.log(1, 
				Thread.currentThread().getStackTrace()[1].getFileName(), 
				Thread.currentThread().getStackTrace()[1].getLineNumber(), 
				message);
			return "";
		}
		catch (FileNotFoundException e) {
			System.out.println(e);
			System.out.println("File not found: "+searchFileName);
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"File not found: "+searchFileName);
			return "";
		} catch (IOException e) {
			System.out.println("Unable to open file: " + searchFileName+
					"; or error accessing Lucene index.");
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Unable to open file: " + searchFileName+" or error accessing Lucene index.");
			return "";
		}	
		
		String jsonResults = "";
		
		try {
			IndexSearcher searcher = new IndexSearcher(reader);
		    
			// Assemble query
			
		    BooleanQuery boolQuery = new BooleanQuery();
		    
		    String[] contentsSplit = contents.split(" ");
		    BooleanQuery.setMaxClauseCount(contentsSplit.length);
		    
		    for (String clauseString : contentsSplit) {
		    	PhraseQuery phrase = new PhraseQuery();
		    	phrase.add(new Term("viswords", clauseString));
		    	boolQuery.add(phrase, Occur.SHOULD);
		    }
		    
		    // this automatically returns them sorted by score, descending
		    TopDocs rawResults = searcher.search(boolQuery, null, config.searchmaxreturn);

		    float maxScore = rawResults.getMaxScore();
		    
		    ArrayList<ScorePercentDoc> results = new ArrayList<ScorePercentDoc>();
		    for (ScoreDoc rawResult : rawResults.scoreDocs) {
		    	Document docc = reader.document(rawResult.doc);
		    	String filename = docc.getField("filename").stringValue();
		    	ScorePercentDoc result = new ScorePercentDoc(rawResult, maxScore, filename);
		    	
		    	if (result.score >= config.searchlucenerelevancethreshold 
		    	&& result.percentScore >= config.searchrelativerelevancethreshold) {
		    		results.add(result);
		    	}
		    }
		    
		    jsonResults = jsonIfyList(results);
		    
		} catch (IOException e) {
			String message = "Unable to access Lucene index.";
			System.out.println(message);
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					message);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// ignore
				}
			}
		}
		
		return jsonResults;
	}
	
	/**
	 * <p>Produce JSON-format string storing results</p>
	 * 
	 * @param	results	The search results to make JSON version of
	 * @return			The JSON-format string representation of results
	 */
	private String jsonIfyList(ArrayList<ScorePercentDoc> results) {
		String jsonized = "[";
		
		for (ScorePercentDoc result : results) {
			jsonized += jsonIfyScorePercentDoc(result) + ", ";
		}
		
		jsonized = jsonized.replaceAll(", $", "") + "]";
		
		return jsonized;
	}
	
	/**
	 * <p>Produce JSON-format string version of the ScorePercentDoc</p>
	 * 
	 * @param	doc	The ScorePercentDoc
	 * @return		The JSON-format version of doc
	 */
	private String jsonIfyScorePercentDoc(ScorePercentDoc doc) {
		String jsonized = "{\"file\": \""+doc.filename.replaceAll("\\.txt$", 
			"")+"\", \"doc\": "+doc.doc+", \"score\": "+doc.score+", \"percent\": "+
			doc.percentScore+"}";

		return jsonized;
	}
	
	
}
