/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */
package com.carlstahmer.archv.indexer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


/**
 * @author cstahmer
 * 
 * <p>The AddBiaCatalogueWords class is an executable class that 
 * add BIA human catalog terms to the visual word files produced 
 * by Arch-V prior to Lucene indexing.</p>
 * 
 * <p>Requires the presence of a config.yml file in the application root to run.</p>
 */
public class AddBiaCatalogueWords {

	Conf config;
	SqlModel sqlObj;
	Logger logger;
	FileUtils fileUtil;
	
	/**
	 * Initializes object's members
	 * 
	 * @param	confObject	configuration -- uses lucenereaddir and lucenewritedir
	 * @param	sqlModObj	Connection to db
	 */
	public AddBiaCatalogueWords(Conf confObject, SqlModel sqlModObj) {
		config = confObject;
		sqlObj = sqlModObj;
		logger = new Logger(config);
		fileUtil = new FileUtils(config, sqlObj);	
	}
	
	/**
	 * <p>creates new Lucene index and fills it with optimized visual word data.</p>
	 * 
	 */
	public void run() {
		
		ArrayList<Integer> arrListDTs = null;
		ArrayList<Integer> arrListGTs = null;
		String contents;
	
		
		if (!requiredConfigPresent()) {
			//System.out.println("Read directory or write directory not configured. Aborting...");
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Read directory or write directory not configured.");
			return;
		}
		
		fileUtil.listFilesWhere(config.listenDir, "txt");
		
		logger.log(3,
			Thread.currentThread().getStackTrace()[1].getFileName(), 
			Thread.currentThread().getStackTrace()[1].getLineNumber(), 
			"Files to index: " + String.valueOf(fileUtil.fileList.size()));
		
		for (String fileName : fileUtil.fileList) {
			contents = "";
			String filePath = config.listenDir + "/" + fileName;
			
			// here i need to check if there is any catalogue data for this item
			
			boolean blnIsCatalogued = false;
			
			String strBaseFileName = fileUtil.removeSuffix(fileName, "txt");
			logger.log(3,
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Checking DB for: " + strBaseFileName);
			
			int impressionID = sqlObj.selectImpressionFileID(strBaseFileName);
			if (impressionID > 0) {
				// if I'm here, it means that this impression is in the system,
				// now check and see if there if there is any actual catalogue
				// data associated with this impression
				//System.out.println("found impression with id: " + impressionID);
				
				arrListDTs = sqlObj.selectImpressionDTs(impressionID);
				if (!arrListDTs.isEmpty()) {
					//System.out.println("found descriptive tags.");
					blnIsCatalogued = true;
				}
				
				arrListGTs = sqlObj.selectImpressionGTs(impressionID);
				if (!arrListGTs.isEmpty()) {
					//System.out.println("found genre terms.");
					blnIsCatalogued = true;
				}
				
			}
			
			try {
				
				// load the contents of the vw file
				contents = fileUtil.openAsString(filePath);
			
				if (blnIsCatalogued) {
				
					logger.log(3,
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							"Adding Catlogoue terms to "+filePath);
		
					//System.out.println("Adding Catlogoue terms to "+filePath);
						
					// if i'm here, I have now loaded the file into the contents string
					// now I need to add the catalogue terms to the file as indicated
					
					if (!arrListDTs.isEmpty()) {
						for (int i=0; i < arrListDTs.size(); i++) {
							for (int ix=0; ix < config.humancataloguefactor; ix++) {
								contents = contents + " DT" + arrListDTs.get(i).toString();
							}
						}
					}

					if (!arrListGTs.isEmpty()) {
						for (int i=0; i < arrListGTs.size(); i++) {
							for (int ix=0; ix < config.humancataloguefactor; ix++) {
								contents = contents + " GT" + arrListGTs.get(i).toString();
							}
						}
					}
					
					logger.log(3,
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							"Appended catalogue items to  "+filePath);			
					
				} else {
					logger.log(3,
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							"Skipping "+filePath+" because no catalogue data is present");
				}
				
				// now here write the value of the contents string to the new output file
				
				boolean fileSave = saveNewFile(fileName, contents);
				if (fileSave) {
					logger.log(3, 
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							"Successfully saved new visual word file "+ config.listenDir + "/"+fileName);
				} else {
					logger.log(1, 
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							"Error saving new visual word file "+ config.listenDir + "/"+fileName);
				}
				
			
			} catch (FileNotFoundException e) {
				logger.log(1, 
						Thread.currentThread().getStackTrace()[1].getFileName(), 
						Thread.currentThread().getStackTrace()[1].getLineNumber(), 
						config.listenDir+"/" +fileName+" not found.");
			} catch (IOException e) {
				e.printStackTrace();
				logger.log(1, 
						Thread.currentThread().getStackTrace()[1].getFileName(), 
						Thread.currentThread().getStackTrace()[1].getLineNumber(), 
						"Unable to read from file "+config.listenDir+"/"+fileName);
			}
		}
		
		
	}
	
	
	
	/**
	 * <p>Makes sure that visual word read dir and write dir are both defined. Does 
	 * not check if specified directories exist.</p>
	 * 
	 * @return	whether they are both defined
	 */
	private boolean requiredConfigPresent() {
		return (config.listenDir != null && config.writeDir != null);
	}
	
	
	
	private boolean saveNewFile(String strFileName, String strFileConts) {
		boolean ret = false;
		
		String filePath = config.writeDir + strFileName;
		try {
			fileUtil.saveString(filePath, strFileConts);
			ret = true;
		}
		catch (java.io.IOException e) {
			e.printStackTrace();
		}

		return ret;
	}

}
