/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */
package com.carlstahmer.archv.indexer;

/**
 * @author cstahmer
 * @author Nigel Pierce
 * 
 * <p>The optimizeVisWordFiles class is an executable class that 
 * optimizes visual word files produced by Arch-V prior to Lucene
 * indexing.</p>
 * 
 * <p>Requires the presence of a config.yml file in the application root to run.</p>
 */
public class OptimizeVisWordFiles {
	
	static Logger logger;
	
	/**
	 * <p>The main class that launches the process.</p>
	 * 
	 * <p>Requires the presence of a config.yml file in the application root to run. 
	 * Values in config.yml can be overwritten at runtime via unix style command line
	 * arguments as defined in the parameters section of this documentation.  The
	 * config.yml file must be in place even if you supply other configuration 
	 * information via the command line.</p>
	 *
	 * @param  	-listendir		/full/directory/path/of/listen/directory
	 * @param  	-writedir  		/full/directory/path/of/output/directory (Deprecated)
	 * @param 	-dbserver		the name of the database server
	 * @param 	-dbname			the database name
	 * @param 	-dbuser			the database user
	 * @param 	-dbpass			the database password
	 * @param	-usethreshold	Max times word can appear to include in optimized word files
	 * @param	-fileoccurrencethreshold	max # of files word can appear in to include in optimized word files
	 * @param	-debug			flag to run in debug mode
	 * @param	-console		flag to run log output to console instead of database
	 * @param 	-frequency		flag to tell the system to run the frequency calculation
	 * @param	-optimizefiles	flag to generate optimized visual word files
	 * @param 	-help			flag to return help text
	 */
	public static void main(String[] args) {
		// Welcome Message
		System.out.println("Starting Arch-V Index Optimizer...");
		
		// Load required configuration YML file
		Conf config = new Conf();
		if (!config.loadConf()) {
			// Don't give up quite yet; we need to display help if requested.
			config.checkArgs(args);
			// Now quit
			System.out.println("Unable to load configuration file");
			System.out.println("Aborting operation!");
			System.exit(0);
		} else {
			System.out.println("Configuration YML successfully loaded...");
		}	
		
	    // Instantiate db object and model
	    SqlModel sqlObj = new SqlModel(config);
	    if (sqlObj.openConnection()) {
	    	System.out.println("DB object and model successfully instantiated...");
	    }
	    
	    // create a logger object
	    logger = new Logger(config);
	    System.out.println("Logger successfully initiated...");
	    if (!config.console) {
	    	System.out.println("Logging to database runlog...");
	    }
	    logger.log(2, Thread.currentThread().getStackTrace()[1].getFileName(), Thread.currentThread().getStackTrace()[1].getLineNumber(), "Application successfully loaded with config, sqlObj, and logger");
	    
	    
		// Override YML configuration with command line args if present
		if (!config.checkArgs(args)) {
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Error  processing command line arguments");
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Aborting application!");
			System.out.println("Aborting operation!");
			System.exit(0);
		} else {	
			logger.log(2, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Command line overrides successfully processed");
		}
		
		// Check listen directory for proper formatting of trailing slash	
	    if (config.listenDir.substring(config.listenDir.length() - 1).equals("/")) {
	    	config.listenDir = config.listenDir.substring(0, config.listenDir.length() - 1);
	    }

	    // calculate & record visword frequency in listen directory
	    if (config.frequencyCalc) {
			System.out.println("Calculating frequencies...");
			logger.log(2,
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Calculating frequencies");
	    	FrequencyCalculator freqCalc = new FrequencyCalculator(config, sqlObj);
	    	freqCalc.calculate();
	    }
	    
	    // Generate optimized visword files
	    if (config.optimizeFiles) {
	    	System.out.println("Generating optimized visual word files...");
			logger.log(2,
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Generating optimized visual word files");
	    	FileOptimizer optimizer = new FileOptimizer(config, sqlObj);
	    	
	    	optimizer.run();
	    }
	   
	    // Make Lucene index
	    if (config.makeluceneindex) {
	    	System.out.println("Making Lucene index...");
			logger.log(2,
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Making Lucene index");
			
	    	MakeLuceneIndex indexMaker = new MakeLuceneIndex(config, sqlObj); 
	    	indexMaker.run();
	    }
	    
	    // Search Lucene index
	    if (config.lucenesearch) {
	    	String message = "Searching Lucene index for visual words in " 
		    	+ config.searchseed;
	    	System.out.println(message);
			logger.log(2,
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					message);
			
			LuceneSearch search = new LuceneSearch(config, sqlObj);
			String results = search.search();
			
			if (results.equals("")) {
				System.out.println("Aborting.");
			} else {
				System.out.println("Results: \n" + 
					results.replaceAll(", \\{", ", \n \\{"));
			}
	    }
	    
	    // Add catalogued items as visual words to visual word file
	    if (config.addcataloguewords) {
	    	String message = "Adding catalogue items as visual words to visual word files.";
	    	System.out.println(message);
			logger.log(2,
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					message);
			
			AddBiaCatalogueWords addCat = new AddBiaCatalogueWords(config, sqlObj);
			addCat.run();

	    }
	    
	    System.out.println("Bye, Bye!");

	}

}
