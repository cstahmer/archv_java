/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */
package com.carlstahmer.archv.indexer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * @author cstahmer
 * @author Nigel Pierce
 * 
 * <p>A class that acts as the SQL query model for the application.  All SQL queries 
 * for the entire application are constructed here.  There should be no SQL anywhere 
 * else in the application.</p>
 */
public class SqlModel {
	
	public Connection conn = null;
	public boolean connOpen = false;
	Conf config;
	String dbserver;
	String dbname;
	String dbuser;
	String dbpass;
	
	/**
	 * <p>Constructor class that assigns values from passed Config object 
	 * to local variables needed to communicate with the database.</p>
	 *
	 * @param  config    an instance of the Conf class
	 */
	public SqlModel(Conf configObj) {
		config = configObj;
		dbserver = configObj.dbserver;
		dbname = configObj.dbname;
		dbuser = configObj.dbuser;
		dbpass = configObj.dbpass;
	}
	
////////////////////////////
// Open and Close Methods //
////////////////////////////
	
	/**
	 * <p>Opens a SQL Connection.</p>
	 */
	public boolean openConnection() {
		
		try {
			
			String strConnString = "jdbc:mysql://"+dbserver+"/"+dbname;
			conn = DriverManager.getConnection(strConnString, dbuser, dbpass);
	        connOpen = true;
			return true;
	    } catch (SQLException ex) {
	        // handle any errors
	        System.out.println("SQLException SqlModel.java openConnection(): " + ex.getMessage());
	        System.out.println("SQLState: " + ex.getSQLState());
	        System.out.println("VendorError: " + ex.getErrorCode());
	        return false;
	    }
		
	}
	
	/**
	 * <p>Closes a SQL Connection.</p>
	 */
	public boolean closeConnection() {
		try {
			conn.close();
			connOpen = false;
			return true;
		} catch (SQLException ex) {
			return false;
		}
		
		
	}
	
/////////////////////////
// Data Model Methods //
////////////////////////
	
	
	/**
	 * <p>Adds count to row in archv_wordfreq, or inserts row if it doesn't exist</p>	
	 * 
	 * @param	word	The word (but actually int) whose presence is to be recorded
	 * @param	count	The number of times that word appeared
	 * @return			<code>true</code> if new row inserted or count added to row; <code>false</code> if error
	 */
	public boolean addWordFreq(int word, int count) {
		Logger logger = new Logger(config);
		logger.log(3, Thread.currentThread().getStackTrace()[1].getFileName(), Thread.currentThread().getStackTrace()[1].getLineNumber(), "In SqlModel.addWordFreq");
		boolean success = false;
		
		if (qSelectGeneric("SELECT * FROM archv_wordfreq WHERE word="+word+" LIMIT 1;").size() > 0) {
			int oldCount = qSelectInt("SELECT frequency FROM archv_wordfreq WHERE word="+word+";");
			String strSql = "UPDATE archv_wordfreq SET frequency="+(oldCount+count)+
					" WHERE word="+word+";";
			success = qUpdate(strSql);
			logger.log(3, Thread.currentThread().getStackTrace()[1].getFileName(), Thread.currentThread().getStackTrace()[1].getLineNumber(), "Update success is: " + success);
		}
		else {
			String strSql = "INSERT INTO archv_wordfreq (word, frequency) VALUES ("+
					word+", "+count+");";
			success = qGenericInsert(strSql);
		}
		return success;
	}

	
	/**
	 * <p>Inserts row of visword, file it's from, and count of that word to archv_words_in_files</p>
	 * 
	 * @param	word	The visual word
	 * @param	file	The name/path of the file it's from
	 * @param	count	The number of times that word appeared
	 * @return			true if success; false if error
	 */
	public boolean insertWordsInFiles(int word, String file, int count) {
		String sqlString = "INSERT INTO `archv_words_in_files` (word, file, count) "+
				"VALUES ("+word+", '"+file+"', "+count+");";
		return qGenericInsert(sqlString);
	}
	
	/**
	 * <p>Deletes all the rows from the specified table. What did you think it does, dingus?</p>
	 * 
	 * @param	tableName	The name of the table to empty.
	 */
	public void emptyTable(String tableName) {
		qUpdate("TRUNCATE `"+tableName+"`;");
	}
	
	/**
	 * <p>Returns frequency column in archv_wordfreq</p>
	 * 
	 * @param	visWord	The visual word whose frequency will be returned
	 * @returns			The value of the frequency column
	 */
	public int selectVisWordCount(int visWord) {
		String strSql = "SELECT `frequency` FROM `archv_wordfreq` WHERE `word`=" + visWord + ";";
		int frequency = qSelectInt(strSql);
		
		return frequency;
	}
	
	/**
	 * <p>Returns number of files visword appears in</p>
	 *
	 * @param	visWord	The visual word to use
	 * @returns			Sum of number of rows it's in in archv_words_in_files
	 */
	public int selectCountFilesWordIsIn(int visWord)
	{
		String strSql = "SELECT COUNT(*) FROM `archv_words_in_files` WHERE word="+visWord+";";
		int occurrences = qSelectInt(strSql);
		
		return occurrences;
	}
	
	/**
	 * <p>Returns number of times a file occurs in archv_words_in_files. Equivalently, the number of distinct visual words a file has.</p>
	 * 
	 * @param	fileName	The file in question
	 * @return				Number of times file is in file column of archv_words_in_files
	 */
	public int selectFileOccurrences(String fileName) {
		String strSql = "SELECT COUNT(*) FROM `archv_words_in_files` WHERE file=\""+fileName+"\";";
		int occurrences = qSelectInt(strSql);
		
		return occurrences;
	}
	
	/**
	 * <p>Returns the id from the bia_impressions table for a given image file.</p>
	 * 
	 * @param	fileName	The file in question
	 * @return				the returned id or 0 if not found
	 */
	public int selectImpressionFileID(String fileName) {
		String strSql = "SELECT BIA_IMP_ID FROM `bia_impressions` WHERE `BIA_IMP_File` LIKE '"+fileName+"';";
		int impression_id = qSelectInt(strSql);
		
		return impression_id;
	}	
	
	/**
	 * <p>Returns the descriptive tag ids from the bia_impDescriptiveTags table 
	 * for a given bia_impression.</p>
	 * 
	 * @param	biaImpId	The file in question
	 * @return				the returned id or 0 if not found
	 */
	public ArrayList<Integer> selectImpressionDTs(int biaImpId) {
		String strSql = "SELECT BIA_IDT_DT_ID FROM `bia_impDescriptiveTags` WHERE `BIA_IDT_IMP_ID` = "+biaImpId+";";
		ArrayList<Integer> retArrayList = qSelectIDs(strSql);
		return retArrayList;
	}	
	
	/**
	 * <p>Returns the genre term ids from the bia_impGenreTerms table 
	 * for a given bia_impression.</p>
	 * 
	 * @param	biaImpId	The file in question
	 * @return				the returned id or 0 if not found
	 */
	public ArrayList<Integer> selectImpressionGTs(int biaImpId) {
		String strSql = "SELECT BIA_IGT_BGT_ID FROM `bia_impGenreTerms` WHERE `BIA_IGT_IMP_ID` = "+biaImpId+";";
		ArrayList<Integer> retArrayList = qSelectIDs(strSql);
		return retArrayList;
	}	
	

//////////////////////
// Logging  methods //
//////////////////////
		
	/**
	 * <p>Writes a new log message to the database runlog table.</p>
	 *
	 * @param	messageType	[1] = error, [2] = info, [3] = debug
	 * @param	fileName	the filename of the file initiating the message
	 * @param	lineNumber	the line number from the file where the message is initiated
	 * @param	messageText	the message
	 */
	public int insertLogMessage(int messageType, String fileName, int lineNumber, String messageText) {
		String strSql = "INSERT INTO runlog " +
				"(type, file, line, message)" +
				" VALUES" + 
				" (" + messageType + ", '" + fileName + "', " + lineNumber + ", '" + messageText + "');";
		int insertId = qInsert(strSql);
		return insertId;
	}


///////////////////////////////	
// Generic SQL query methods //
///////////////////////////////
	
	/**
	 * <p>A genreic object for querying the db for a single numeric
	 * value such as an id field, etc.  Field select list must
	 * contain only a single field.</p>
	 *
	 * @param	strSql	A well formed SQL SELECT query with a single SELECT field of type INTEGER
	 * @return			The integer value of the return column
	 */	
	private int qSelectInt(String strSql) {
		
		if (!connOpen) {
			this.openConnection();
		}
		
		// initialize required objects
		Statement stmt = null;
		ResultSet resultSet = null;
		int retId = 0;

		
		// run query
		try {
			stmt = conn.createStatement();
	        resultSet = stmt.executeQuery(strSql);
	        if (resultSet.next()) {
	        	retId = resultSet.getInt(1);
	        }
	        try {
	        	resultSet.close();
	        } catch (SQLException sqlEx) { 
			    // handle any errors
			    System.out.println("SQLException SqlModel.java qSelectInt-A: " + sqlEx.getMessage());
			    System.out.println("SQLState: " + sqlEx.getSQLState());
			    System.out.println("VendorError: " + sqlEx.getErrorCode());	        	
	        } 
		    
		
		} catch (SQLException ex){
		    // handle any errors
		    System.out.println("SQLException SqlModel.java qSelectInt: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}	
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return retId;
	}
	
	
	
	/**
	 * <p>A generic object for querying the db for a single numeric
	 * value such as an id field, etc.  Field select list must
	 * contain only a single field.</p>
	 *
	 * @param	strSql	A well formed SQL SELECT query with a single SELECT field of type INTEGER
	 * @return			The integer value of the return column
	 */	
	private double qSelectDouble(String strSql) {
		
		if (!connOpen) {
			this.openConnection();
		}
		
		// initialize required objects
		Statement stmt = null;
		ResultSet resultSet = null;
		double retVal = 0;

		
		// run query
		try {
			stmt = conn.createStatement();
	        resultSet = stmt.executeQuery(strSql);
	        if (resultSet.next()) {
	        	retVal = resultSet.getLong(1);
	        }
	        try {
	        	resultSet.close();
	        } catch (SQLException sqlEx) { } // ignore
		    
		
		} catch (SQLException ex){
		    // handle any errors
		    System.out.println("SQLException SqlModel.java qSelectLong: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
		    try {
		    	stmt.close();
		    } catch (SQLException sqlEx) { } // ignore

		}
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return retVal;
	}
	
	
	
	
	/**
	 * <p>A genreic object for querying the db for a single String
	 * value. Field select list must contain only a single field.</p>
	 *
	 * @param  	strSql	A well formed SQL SELECT query with a single SELECT field of type STRING
	 * @return			The string value of the return column
	 */		
	private String qSelectString(String strSql) {
		
		if (!connOpen) {
			this.openConnection();
		}
		
		// initialize required objects
		Statement stmt = null;
		ResultSet resultSet = null;
		String retString = "";

		
		// run query
		try {
			stmt = conn.createStatement();
	        resultSet = stmt.executeQuery(strSql);
	        if (resultSet.next()) {
	        	retString = resultSet.getString(1);
	        }
	        try {
	        	resultSet.close();
	        } catch (SQLException sqlEx) { } // ignore
		    
		
		} catch (SQLException ex){
		    // handle any errors
		    System.out.println("SQLException SqlModel.java qSelectString: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
		    try {
		    	stmt.close();
		    } catch (SQLException sqlEx) { } // ignore

		}
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return retString;
	}
	
	/**
	 * <p>A generic object for executing a SELECT querying 
	 * against the db.</p>
	 *
	 * @param  	strSql	A well formed SQL SELECT query
	 * @return			A resultSet object containing the result of the query
	 */	
	private ArrayList<HashMap<String,String>> qSelectGeneric(String strSql) {
		
		
		ArrayList<HashMap<String,String>> retList = new ArrayList<HashMap<String,String>>();
		
		if (!connOpen) {
			this.openConnection();
		}
		
		// initialize required objects
		Statement stmt = null;
		ResultSet resultSet = null;
		ResultSetMetaData rsmd = null;

		
		// run query
		try {
			stmt = conn.createStatement();
	        resultSet = stmt.executeQuery(strSql);	
	        rsmd = resultSet.getMetaData();
	        int colCount = rsmd.getColumnCount();
        	while (resultSet.next()) {
		        for (int i=1; i < (colCount + 1); i++) {
		        	HashMap<String, String> fieldHash = new HashMap<String, String>();
		            fieldHash.put(rsmd.getColumnName(i), resultSet.getString(i));
		            retList.add(fieldHash);
		        }
        	}
	        try {
	        	resultSet.close();
	        } catch (SQLException sqlEx) { } // ignore
		} catch (SQLException ex){
		    // handle any errors
		    System.out.println("SQLException SqlModel.java qSelectGeneric: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}	
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return retList;
	}
	
	/**
	 * <p>A generic object for executing an UPDATE 
	 * against the db.</p>
	 *
	 * @param  	strSql	A well formed SQL UPDATE query
	 * @return			A boolean value indicating success or failure
	 */		
	private boolean qUpdate(String strSql) {
		
		if (!connOpen) {
			this.openConnection();
		}
		
		// initialize required objects
		boolean success = false;
		Statement stmt = null;
		
		try {

		    stmt = conn.createStatement();
		    stmt.executeUpdate(strSql);
		    success = true;   
		} catch (SQLException ex){
			    // handle any errors
			    System.out.println("SQLException SqlModel.java qUpdate: " + ex.getMessage());
			    System.out.println("SQLState: " + ex.getSQLState());
			    System.out.println("VendorError: " + ex.getErrorCode());

		} finally {

		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException ex) {
		            // ignore
		        }
		    }
		}
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return success;
	}
	
	
	/**
	 * <p>A generic object for executing an INSERT 
	 * against the db.</p>
	 *
	 * @param  	strSql	A well formed SQL SELECT query
	 * @return			An int value indicating success or failure for auto-increment row
	 */
	private int qInsert(String strSql) {
		
		// initialize required objects
		Statement stmt = null;
		ResultSet rs = null;
		int insertId = 0;
		
		if (!connOpen) {
			this.openConnection();
		}

		try {

		    stmt = conn.createStatement();
		    stmt.executeUpdate(strSql);
		    rs = stmt.executeQuery("SELECT LAST_INSERT_ID()"); // this part is only zero if 
		    	//row is not auto-incrementing, or failure. CANNOT BE DEPENDED ON TO DETERMINE 
		    	// FAILURE.
		    if (rs.next()) {
		    	insertId = rs.getInt(1);
		    }
		    rs.close();
		    
		} catch (SQLException ex){
			    // handle any errors
			    System.out.println("SQLException SqlModel.java qInsert: " + ex.getMessage());
			    System.out.println("SQLState: " + ex.getSQLState());
			    System.out.println("VendorError: " + ex.getErrorCode());

		} finally {

		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException ex) {
		            // ignore
		        }
		    }
		}
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return insertId;
	}	
	
	/**
	 * <p>A generic object for executing an INSERT 
	 * against the db -- no auto-increment col required.</p>
	 *
	 * @param  	strSql	A well formed SQL SELECT query
	 * @return			A bool value indicating success or failure (failure on error)
	 */
	private boolean qGenericInsert(String strSql) {
		
		// initialize required objects
		Statement stmt = null;
		boolean success = false;
		
		if (!connOpen) {
			this.openConnection();
		}

		try {

		    stmt = conn.createStatement();
		    stmt.executeUpdate(strSql);
		    success = true;
		    
		} catch (SQLException ex){
			    // handle any errors
			    System.out.println("SQLException SqlModel.java qGenericInsert: " + ex.getMessage());
			    System.out.println("SQLState: " + ex.getSQLState());
			    System.out.println("VendorError: " + ex.getErrorCode());
			    success = false;

		} finally {

		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException ex) {
		            // ignore
		        }
		    }
		}
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return success;
	}
	
	
	/**
	 * <p>A generic object for executing a SELECT querying 
	 * against the db and returning a list of found ID's/Integers</p>
	 *
	 * @param  	strSql	A well formed SQL SELECT query that returns only one numeric field
	 * @return			A resultSet object containing the result of the query
	 */	
	private ArrayList<Integer> qSelectIDs(String strSql) {
		
		
		ArrayList<Integer> retList = new ArrayList<Integer>();
		
		if (!connOpen) {
			this.openConnection();
		}
		
		// initialize required objects
		Statement stmt = null;
		ResultSet resultSet = null;
		//ResultSetMetaData rsmd = null;

		
		// run query
		try {
			stmt = conn.createStatement();
	        resultSet = stmt.executeQuery(strSql);	
	        //rsmd = resultSet.getMetaData();
        	while (resultSet.next()) {
        		retList.add(resultSet.getInt(1));
        	}
	        try {
	        	resultSet.close();
	        } catch (SQLException sqlEx) { } // ignore
		} catch (SQLException ex){
		    // handle any errors
		    System.out.println("SQLException SqlModel.java qSelectGeneric: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}	
		
		if (connOpen) {
			this.closeConnection();
		}
		
		return retList;
	}
	
}
