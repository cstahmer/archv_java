/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */
package com.carlstahmer.archv.indexer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * @author cstahmer
 * @author Nigel Pierce
 * 
 * <p>A class for performing visual word frequency calculations.</p>
 */
public class FrequencyCalculator {
	
	Conf config;
	SqlModel sqlObj;
	Logger logger;
	
	/**
	 * <p>Constructor class that assigns passed Config object and SqlModel 
	 * to class instances and creates a Logger class instance.</p>
	 *
	 * @param  configObj    an instance of the Conf class
	 * @param  sqlModObj  	an instance of the sqlModel class
	 */
	public FrequencyCalculator(Conf configObj, SqlModel sqlModObj) {
		config = configObj;
		sqlObj = sqlModObj;
		logger = new Logger(config);
	}

	/*
	 * PUT METHODS HERE.  USE FULL JAVADOC
	 */
	
	/*
	 * OK. :)
	 */
	
	/**
	 * <p>Calculates and stores in DB the frequencies of each visual word, and the files they appear in.</p>
	 */
	public void calculate() {
		
		boolean noFatalError = true;
		
		// clear out archv_wordfreq and archv_words_in_files
		sqlObj.emptyTable("archv_wordfreq");
		sqlObj.emptyTable("archv_words_in_files");
		
		// loop through the files in listendir
		FileUtils files = new FileUtils(config, sqlObj);
		files.listFilesForFolder(config.listenDir);
		for (int iter = 0; iter < files.fileList.size() && noFatalError; iter++) {
			try {
				// make sure it's a txt file
				if (!files.fileType(files.fileList.get(iter)).equals("txt")) {
					logger.log(1, 
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							files.fileList.get(iter)+" is not a text file.");
					continue;
				} else {
					logger.log(3, 
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							"Calculating frequencies in file " + files.fileList.get(iter));
				}
				
				// load contents of file
				String strVisWords = "";
				strVisWords += files.openAsString(config.listenDir + "/" + 
						files.fileList.get(iter));
				
				// get quantity of each visual word
				ArrayList<String> visWords = new ArrayList<String>(Arrays.asList(strVisWords.split(" ")));
				if (visWords.size() > 0 && visWords.get(0).equals("EMBLEM")) {
					visWords.clear();
					visWords.add("0");
				}
				HashMap<Integer, Integer> wordCounter = new HashMap<Integer, Integer>();
				for(String visWord : visWords) {
					int intOfWord = Integer.valueOf(visWord);
					wordCounter.put(intOfWord, zerofyNull(wordCounter.get(
							intOfWord)) + 1);
				}
				
				// store in db
				for (Entry<Integer, Integer> wordAndCount : wordCounter.entrySet()) {
					// add quantity to appropriate row of archv_wordfreq
					int visWord = wordAndCount.getKey();
					int count = wordAndCount.getValue();
					noFatalError = sqlObj.addWordFreq(visWord, count);
					
					// record word, file, and count to arcv_words_in_files
					noFatalError = noFatalError && sqlObj.insertWordsInFiles(visWord, 
							files.fileList.get(iter), 
							count);
					
					if (!noFatalError) {
						logger.log(1, 
							Thread.currentThread().getStackTrace()[1].getFileName(), 
							Thread.currentThread().getStackTrace()[1].getLineNumber(), 
							"SQL error when recording word frequencies. Check console output.");
						System.out.println("Fatal error; exiting.");
						break;
					}
				}
			}
			catch (FileNotFoundException e) {
				// who knows how this would happen.
				e.printStackTrace();
				logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					files.fileList.get(iter)+" not found.");
			}
			catch (IOException e) {
				// or this.
				e.printStackTrace();
				logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Unable to read from file "+files.fileList.get(iter));
			}
			catch (java.lang.NumberFormatException e) {
				logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					files.fileList.get(iter)+" is invalid: contains non-numeric string.");
			}
			
		}
		
		
		logger.log(3, 
				Thread.currentThread().getStackTrace()[1].getFileName(), 
				Thread.currentThread().getStackTrace()[1].getLineNumber(), 
				"Finished Processing Files");
	}
	
	
	
	/**
	 * <p>Converts null to 0; otherwise returns given value.</p>
	 * 
	 * @param mightBeNull	The <code>Integer</code> that might be null.
	 * @return				0 if given int is null; the given int's value otherwise.
	 */
	private int zerofyNull(Integer mightBeNull) {
		if (mightBeNull == null) {
			return 0;
		}
		else {
			return mightBeNull;
		}
	}

}
