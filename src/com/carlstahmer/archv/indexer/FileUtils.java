/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanities.</p>
 */
package com.carlstahmer.archv.indexer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * @author cstahmer
 * @author Nigel Pierce
 * 
 * <p>A utility class for performing various operations on file-system objects.</p>
 */
public class FileUtils {
	
	Conf config;
	SqlModel sqlObj;
	Logger logger;
	public ArrayList<String> fileList = new ArrayList<String>();
	public ArrayList<String> directoryList = new ArrayList<String>();
	
	/**
	 * <p>Constructor class that assigns passed Config object and SqlModel to 
	 * class instances.</p>
	 *
	 * @param  configObj    an instance of the Conf class
	 * @param  sqlModObj  	an instance of the sqlModel class
	 */
	public FileUtils(Conf configObj, SqlModel sqlModObj) {
		config = configObj;
		sqlObj = sqlModObj;
		logger = new Logger(config);
		directoryList.clear();
		fileList.clear();
	}
	
	/**
	 * <p>Constructor that doesn't need a SqlModel because why would this class need one</p>
	 * 
	 * @param	configObj	configuration to have
	 */
	//public FileUtils(Conf configObj) {
	//	config = configObj;
	//	logger = new Logger(config);
	//}
	
	
	/**
	 * <p>Traverses a designated directory recursively and returns a 
	 * list of all files found in the directory path.</p>
	 *
	 * @param  folderDir    The directory to search
	 */
	public void listFilesRecursive(String folderDir) {
		final File folder = new File(folderDir);
		fileList.clear();
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry.getName());
	        } else {
	        	String thisFileName = fileEntry.getName();
	        	if (!thisFileName.equals(".") && !thisFileName.equals("..")) {
	        		fileList.add(fileEntry.getName());
	        	}
	        }
	    }
	}
	
	/**
	 * <p>Initiates process of generating list of institutional directories to traverse.</p>
	 *
	 * @param  folderDir    The directory to search
	 */
	public void listFoldersRecursive() {
		directoryList.clear();
		String folderToListen = config.listenDir;
		listFolders(folderToListen);
	}
	
	/**
	 * <p>Produces a recursive list of directories found in a designated directory path.</p>
	 *
	 * @param  folderDir    The directory to search
	 */
	public void listFolders(String folderDir) {
		final File folder = new File(folderDir);
	    for (final File dirEntry : folder.listFiles()) {
	        if (dirEntry.isDirectory()) {
	        	String thisFileName = dirEntry.getName();
	        	if (!thisFileName.equals(".") && !thisFileName.equals("..")) {
	        		directoryList.add(thisFileName);
	        		listFolders(folderDir+"/"+thisFileName);
	        	}
	        }
	    }
	    logger.log(3, Thread.currentThread().getStackTrace()[1].getFileName(), Thread.currentThread().getStackTrace()[1].getLineNumber(), "Size of directoryList array: "+String.valueOf(directoryList.size()));
	}
	
	/**
	 * <p>Produces a non-recursive list of files found in a designated directory.</p>
	 *
	 * @param  folderDir    The directory to search
	 */	
	public void listFilesForFolder(String folderDir) {
		final File folder = new File(folderDir);
		fileList.clear();
	    for (final File fileEntry : folder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	        	String thisFileName = fileEntry.getName();
	        	if (!thisFileName.equals(".") && !thisFileName.equals("..")) {
	        		fileList.add(fileEntry.getName());
	        	}
	        }
	    }
	}
	

	/**
	 * <p>Examines a file name and returns the file type suffix.</p>
	 *
	 * @param  	strFileName		the filename to check
	 * @return					a string representing the file-type suffix of the file		
	 */	
	public String fileType(String strFileName) {
		String ret = "";
		
		
		String[] parts = strFileName.split("\\.");
		int sizeParts = parts.length;
		String fileSuffix = parts[(sizeParts - 1)];
		if (fileSuffix.length() > 0) {
			ret = fileSuffix;
		}	
	
		return ret;
	}
	
	/**
	 * <p>A simple utility for comparing two strings.</p>
	 *
	 * @param	testString	The hay-stack to search
	 * @param	testCase	The needle to search for
	 * @return				A boolean comparison result [true/false]	
	 */		
	public boolean matchRegex(String testString, String testCase) {
		boolean blnMatch = false;
		Pattern pattern = Pattern.compile("[.]"+testCase+"$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(testString);

        while (matcher.find()) {
        	blnMatch = true;
        }
		return blnMatch;
	}
	
	/**
	 * <p>Opens a text file and returns its contents as a string</p>
	 * 
	 * @param	strFileName	Name/path of file to open
	 * @return	A String of the contents of the file
	 * @throws	FileNotFoundException 
	 * @throws	IOException
	 */
	public String openAsString(String strFileName) throws FileNotFoundException, IOException {
		BufferedReader reader = null;
		String contents = "";
		try {
			reader = new BufferedReader(new FileReader(strFileName));
			contents = reader.readLine();
			reader.close();
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (IOException e) {
			reader.close();
			throw e;
		}
		
		return contents;
	}
	

	/**
	 * <p>Saves a string into a new text file. Creates new folders and file, if don't already exist.</p>
	 * 
	 * @param	strFileName		path to the text file
	 * @param	content			The string to save
	 * @throws 	IOException
	 */
	public void saveString(String strFileName, String content) throws 
	SecurityException, IOException {
		BufferedWriter writer = null;
		
		// check that the directories exist...
		String directoryPath = strFileName.substring(0, Math.max(0, strFileName.lastIndexOf('/')));
		File directory = new File(directoryPath);
		if (!directory.exists() || directory.isFile()) {
			// and make it if it doesn't exist.
			try {
				boolean madeDirs = directory.mkdirs();
				System.out.println("Successfully made directories: " + madeDirs);
				if (!madeDirs) {
					throw new IOException("Cannot create directory: " + 
							directoryPath + "; name already exists as file.");
				}
			} catch (SecurityException e) {
				throw e;
			}
		}
		
		try {
			writer = new BufferedWriter(new FileWriter(strFileName));
			writer.write(content);
			writer.close();
		}
		catch (IOException e) {
			if (writer != null) {
				writer.close();
			}
			throw e;
		}
		
	}
	
	public void listFilesWhere(String folderDir, String extension) {
		listFilesForFolder(folderDir);
		
		ArrayList<String> newFileList = new ArrayList<String>();
		for (String fileName : fileList) {
			if (fileType(fileName).equals(extension)) {
				newFileList.add(fileName);
			}
		}
		
		fileList = newFileList;
	}
	
	
	/**
	 * <p>Reuturns the filename without the designated suffix.</p>
	 * 
	 * @param	strFileName		path to the text file
	 * @param	strSuffix			The string to save
	 * 
	 * @return					the truncated filename
	 */
	public String removeSuffix(String strFileName, String strSuffix) {
		String retFileName = "";
		String strRegexPattern = "\\."+strSuffix;
		String strReplacePattern = "."+strSuffix;
		Pattern suffixPattern = Pattern.compile(strRegexPattern, Pattern.CASE_INSENSITIVE);
		if (suffixPattern.matcher(strFileName).find()) {
			retFileName = strFileName.replaceAll(strReplacePattern, "");
		} else {
			retFileName = strFileName;
		}
		return retFileName;
	}
	
}
