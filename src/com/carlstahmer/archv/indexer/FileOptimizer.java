/**
 *	<p>Copyright (c) 2014, Carl Stahmer - <a href="http://www.carlstahmer.com">www.carlstahmer.com</a>.</p>
 *	
 *	<p>This file is part of the BIA/Archive-Vision Image Indexing
 *  platform.  It provides a collection of methods for optimizing
 *  visual word representations of images and for interacting
 *  with lucene to produce a structured, mySQL representation of 
 *  relationships between images in the indexed library.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software are free software: you can redistribute it 
 *	and/or modify it under the terms of the GNU General Public License 
 *	as published by the Free Software Foundation, either version 3 of 
 *	the License, or (at your option) any later version.</p>
 *
 *	<p>Archive Vision (Arch-V) and all its components, including this
 *  software is distributed in the hope that it will 
 *	be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 *	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *	GNU General Public License for more details.</p>
 *
 *	<p>You should have received a copy of the GNU General Public License  
 *	along with the ESTC Record Importer distribution.  If not, 
 *	see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 *	<p>Development of this software was made possible through funding from 
 *	the National Endowment for the Humanties.</p>
 */
package com.carlstahmer.archv.indexer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Nigel Pierce
 *
 * <p>Class that outputs visual word files containing only rare / uncommon 
 * visual words</p> 
 */
public class FileOptimizer {
	
	Conf config;
	SqlModel sqlObj;
	Logger logger;
	FileUtils fileUtil;
	
	/**
	 * <p>Constructor class that assigns passed Config object and SqlModel 
	 * to class instances and creates a Logger class instance.</p>
	 *
	 * @param  configObj    an instance of the Conf class
	 * @param  sqlModObj  	an instance of the sqlModel class
	 */
	public FileOptimizer(Conf configObj, SqlModel sqlModObj) {
		config = configObj;
		sqlObj = sqlModObj;
		logger = new Logger(config);
		fileUtil = new FileUtils(config, sqlObj);
	}
	
	/**
	 * <p>Generates optimized visual word files from originals specified in config</p>
	 */
	public void run() {
		
		// loop through the files in listendir
		fileUtil.listFilesForFolder(config.listenDir);
		for (String currentFile : fileUtil.fileList){
			System.out.println("Optimizing "+currentFile);
			
			// make sure it's a txt file
			if (!fileUtil.fileType(currentFile).equals("txt")) {
				logger.log(1, 
						Thread.currentThread().getStackTrace()[1].getFileName(), 
						Thread.currentThread().getStackTrace()[1].getLineNumber(), 
						currentFile+" is not a text file.");
				continue;
			}
			// make sure word frequencies have been calculated for it
			if (!isFileFrequencyCalculated(currentFile)) {
				logger.log(1, 
						Thread.currentThread().getStackTrace()[1].getFileName(), 
						Thread.currentThread().getStackTrace()[1].getLineNumber(), 
						"Word frequencies in "+currentFile+
						" have not yet been calculated; skipping file.\n  " + 
						"(Try running -frequency again before optimizing to resolve this error.)");
				continue;
			}
			
			// make list of optimized viswords
			ArrayList<String> optimizedWords = new ArrayList<String>();
			try {
				
				optimizedWords = optimizeFile(currentFile);
				
			}
			catch (NumberFormatException e) {
				// ignore
				continue;
			}
			catch (IOException e) {
				// ignore
				continue;
			}
			
			// save optimized version
			try {
				
				saveOptimizedWords(currentFile, optimizedWords);
				
			} catch (IOException e) {
				continue; // ignore
			}
		}
	}

	/**
	 * <p>Returns viswords that meet optimization criteria</p>
	 * 
	 * @param	file			The name of the file containing viswords
	 * @return					The viswords (could be a set, but whatever)
	 * @throws	IOException
	 */
	private ArrayList<String> optimizeFile(String file)	throws java.lang.NumberFormatException, java.io.IOException {
		
		ArrayList<String> optimized = new ArrayList<String>();
		HashMap<Integer, Boolean> boolWordsInThresholds = new HashMap<Integer, Boolean>(); 
		String filePath = config.listenDir + "/" + file;
		String contents = "";
		
		try {
			contents = fileUtil.openAsString(filePath);
			logger.log(2, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Optimizing file: " + filePath);
		}
		catch (java.io.IOException e) {
			logger.log(1, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Unable to open file: " + filePath);
			throw e;
		}
		
		// account for empty file
		if (contents == null) {
			logger.log(3, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					filePath + " is empty.");
			return optimized;
		}
		
		try {
			for (String strWord : contents.split(" ")) {
				
				// if file has EMBLEM, just record "0" & nothing else.
				if (strWord.equals("EMBLEM")) {
					optimized.clear();
					optimized.add("0");
					break;
				}
				
				// otherwise do the number stuff
				Integer visWord = Integer.valueOf(strWord);
				
				// record word if it's not been encountered
				if (!boolWordsInThresholds.containsKey(visWord)) {
					boolWordsInThresholds.put(visWord, 
							withinThresholds(visWord));
				}
				
				// keep it if it's within thresholds
				if (boolWordsInThresholds.get(visWord) == true) {
					optimized.add(strWord);
				}
				
			}
		}
		catch (java.lang.NumberFormatException e) {
			logger.log(1, 
				Thread.currentThread().getStackTrace()[1].getFileName(), 
				Thread.currentThread().getStackTrace()[1].getLineNumber(), 
				filePath+" is invalid: contains non-numeric string.");
			throw e;
		}
		
		return optimized;
	}

	/**
	 * <p>Saves supplied visWords to file at supplied path</p>
	 * 
	 * @param	filename		Path to the file
	 * @param	optimizedWords	The words to save
	 * @return					Whether it was successful
	 * @throws	IOException 
	 */
	private boolean saveOptimizedWords(String filename, ArrayList<String> optimizedWords) throws IOException {
		
		boolean success = false;
		
		// make optimized file's contents
		String stringWords = "";
		for (String word : optimizedWords) {
			stringWords = stringWords + word + " ";
		}
		if (stringWords.length() > 0) {
			// remove trailing space 
			stringWords = stringWords.substring(0, stringWords.length() - 1);
		}
		else {
			// only contains "0" if no word passes thresholds
			stringWords = "0";
		}
	
		// save the darn contents
		String filePath = config.writeDir + filename;
		try {
			fileUtil.saveString(filePath, stringWords);
			success = true;
			logger.log(2, 
					Thread.currentThread().getStackTrace()[1].getFileName(), 
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Output " + filePath);
		}
		catch (java.io.IOException e) {
			e.printStackTrace();
			logger.log(1,
					Thread.currentThread().getStackTrace()[1].getFileName(),
					Thread.currentThread().getStackTrace()[1].getLineNumber(), 
					"Could not save to file: " + filePath + "; check console output.");
			throw e;
		}
		
		return success;
	}
	
	/**
	 * <p>Returns whether supplied word meets criteria to be "optimal"</p>
	 * 
	 * @param			visWord	The word to check
	 * @return			true if appears below a certain number of times, and below a certain 
	 * 					number of files
	 */
	private boolean withinThresholds(int visWord) {
		
		if (sqlObj.selectVisWordCount(visWord) < config.usethreshold && sqlObj.selectCountFilesWordIsIn(visWord) < config.fileoccurrencethreshold) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * <p>Returns whether specified file in read folder has been frequency-calculated 
	 * (whether it is in the "file" column of archv_words_in_files directory)</p>
	 * 
	 * @param	currentFile	The file to check
	 * @return				true if is in archv_words_in_files, false otherwise.
	 */
	private boolean isFileFrequencyCalculated(String currentFile) {
		return sqlObj.selectFileOccurrences(currentFile) > 0;
	}
	
}
