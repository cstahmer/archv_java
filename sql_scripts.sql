-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 06, 2014 at 01:34 PM
-- Server version: 5.6.11
-- PHP Version: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


-- --------------------------------------------------------

--
-- Table structure for table `archv_wordfreq`
--

CREATE TABLE IF NOT EXISTS `archv_wordfreq` (
  `word` int(11) NOT NULL,
  `frequency` bigint(20) NOT NULL,
  KEY `frequency` (`frequency`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archv_words_in_files`
--

CREATE TABLE IF NOT EXISTS `archv_words_in_files` (
  `word` int(11) NOT NULL,
  `file` varchar(200) NOT NULL,
  `count` int(11) NOT NULL,
  KEY `file` (`file`,`count`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `runlog`
--

CREATE TABLE IF NOT EXISTS `runlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `file` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `line` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `runlog_message_types`
--

CREATE TABLE IF NOT EXISTS `runlog_message_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `runlog_message_types`
--

INSERT INTO `runlog_message_types` (`id`, `type`) VALUES
(1, 'error'),
(2, 'info'),
(3, 'debug');


-- --------------------------------------------------------

--
-- Table structure for table `bia_archv`
--

CREATE TABLE IF NOT EXISTS `bia_archv` (
  `BAV_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'System Record ID',
  `BAV_Seed` bigint(20) NOT NULL COMMENT 'The seed',
  `BAV_Match` bigint(20) NOT NULL COMMENT 'The Match',
  `BAV_Relevance` decimal(12,8) NOT NULL COMMENT 'the relevance',
  PRIMARY KEY (`BAV_ID`),
  KEY `BAV_Seed` (`BAV_Seed`,`BAV_Relevance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
