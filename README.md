# README #

Archv_java is a collection of tools for optimizing and creating Lucene indexes of buckets of visual words (BOVW) 
of images that have been created using the archv tool (https://bitbucket.org/cstahmer/archv) as well as providing 
a JSON interface for searching these indexes. The combined toolset is being developed in an effort to provide scalable 
visual search capability for archives of printed materials. Development of the codebase was initially supported by a 
Start-Up Grant from the National Endowment for the Humanities (NEH.) Continued development is a labor of love and 
necessity (pending future funding.)

The code is made available under a creative commons Attribution Share-Alike (CC BY-SA 4.0) License
https://creativecommons.org/licenses/by-sa/4.0/

The codebase is currently under heave development and there is presently no user documentation. This is one of the 
things we hope to provide with future funding. For information of how to implement or contribute, contact Carl Stahmer 
at cstahmer@ucdavis.edu

Carl G Stahmer
Director of Digital Scholarship
University of California Davis Library
www.carlstahmer.com
@cstahmer